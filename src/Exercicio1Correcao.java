
public class Exercicio1Correcao {

	public static void main(String[] args) {
		exibeSaudacao(3);
		exibeSaudacao(15);
		exibeSaudacao(23);
	}
	
	static void exibeSaudacao(int hora) {
		
		if (hora < 0) {
			System.out.println("Horario invalido");
		} else if (hora < 12) {
			System.out.println("Bom dia");
		} else if (hora < 18) {
			System.out.println("Boa tarde");
		} else if (hora < 24) {
			System.out.println("Boa noite");
		} else {
			System.out.println("Horario invalido");
		}
	}
}
