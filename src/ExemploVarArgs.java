
public class ExemploVarArgs {

	static void exibirNome(String... nomes) {
		for (String n : nomes) {
			System.out.println(n);
		}
	}
	
	public static void main(String[] args) {
		exibirNome("Zeca", "João", "Manuel");
	}
}
